#!/usr/bin/python
# -*- coding: utf-8 -*-

from pygame import Surface, Color

def render_keyboard(SCR, X, Y, KEYS):
	simple_key = Surface(( 50, 50)); simple_key.fill(Color("#777777"))
	tab_key    = Surface(( 75, 50));    tab_key.fill(Color("#777777"))
	caps_key   = Surface(( 85, 50));   caps_key.fill(Color("#777777"))
	ctrl_key   = Surface(( 60, 50));   ctrl_key.fill(Color("#777777"))
	w_key      = Surface(( 50, 50));
	s_key      = Surface(( 50, 50));
	a_key      = Surface(( 50, 50));
	d_key      = Surface(( 50, 50));
	r_key      = Surface(( 50, 50));
	f_key      = Surface(( 50, 50));
	shift_key  = Surface((120, 50));
	space_key  = Surface((290, 50));

	if KEYS['W']: w_key.fill(Color("#FF7700"))
	else:         w_key.fill(Color("#777777"))
	if KEYS['S']: s_key.fill(Color("#FF7700"))
	else:         s_key.fill(Color("#777777"))
	if KEYS['D']: d_key.fill(Color("#FF7700"))
	else:         d_key.fill(Color("#777777"))
	if KEYS['A']: a_key.fill(Color("#FF7700"))
	else:         a_key.fill(Color("#777777"))
	if KEYS['R']: r_key.fill(Color("#FF7700"))
	else:         r_key.fill(Color("#777777"))
	if KEYS['F']: f_key.fill(Color("#FF7700"))
	else:         f_key.fill(Color("#777777"))
	if KEYS['Shift']: shift_key.fill(Color("#FF7700"))
	else:             shift_key.fill(Color("#777777"))
	if KEYS['Space']: space_key.fill(Color("#FF7700"))
	else:             space_key.fill(Color("#777777"))

	#===== 1 =====#
	for i in range(8): SCR.blit(simple_key, (X + i*50 + i*10,       Y +   0))
	#===== 2 =====#
	SCR.blit(tab_key,    (X +   0, Y +  60))
	SCR.blit(simple_key, (X +  85, Y +  60))
	SCR.blit(w_key,      (X + 145, Y +  60))
	SCR.blit(simple_key, (X + 205, Y +  60))
	SCR.blit(r_key,      (X + 265, Y +  60))
	for i in range(3): SCR.blit(simple_key, (X + 325 + i*50 + i*10, Y +  60))
	#===== 3 =====#
	SCR.blit(caps_key,   (X +   0, Y + 120))
	SCR.blit(a_key,      (X +  95, Y + 120))
	SCR.blit(s_key,      (X + 155, Y + 120))
	SCR.blit(d_key,      (X + 215, Y + 120))
	SCR.blit(f_key,      (X + 275, Y + 120))
	for i in range(3): SCR.blit(simple_key, (X + 335 + i*50 + i*10, Y + 120))
	#===== 4 =====#
	SCR.blit(shift_key,  (X +   0, Y + 180))
	for i in range(7): SCR.blit(simple_key, (X + 130 + i*50 + i*10, Y + 180))
	#===== 5 =====#
	SCR.blit(ctrl_key,   (X +   0, Y + 240))
	for i in range(3): SCR.blit(simple_key, (X +  70 + i*50 + i*10, Y + 240))	
	SCR.blit(space_key,  (X + 250, Y + 240))
	#=============#
