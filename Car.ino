#include <SoftwareSerial.h>

//===== Protocol =====//
#define STX 0x02
#define ETX 0x03

//===== Serial =====//
#define RX_PIN 2
#define TX_PIN 3
#define SET_PIN 4

//===== Motor driver =====//
#define FWD_PIN 9
#define BWD_PIN 8
#define L_PIN 10
#define R_PIN 11

#define MSG_LEN 6
byte msg[MSG_LEN]; // STX, L/R, DIR, PWR, ETX, CRC, ENT

bool LATCH = 0;
unsigned long tim = millis();

SoftwareSerial mySerial(RX_PIN, TX_PIN);

void setup() {
  Serial.begin(115200);
 
  pinMode(SET_PIN, OUTPUT);
  digitalWrite(SET_PIN, 1);

  mySerial.begin(9600);
}

void motorSetup (int LR, int DIR, int PWR) {
  if (LR == 0) {
    digitalWrite(L_PIN, 1);
    digitalWrite(R_PIN, 0);
  }
  if (LR == 127) {
    digitalWrite(L_PIN, 0);
    digitalWrite(R_PIN, 0);
  }
  if (LR == 255) {
    digitalWrite(L_PIN, 0);
    digitalWrite(R_PIN, 1);
  }
  
  if (DIR == 0) {
    analogWrite (FWD_PIN, PWR);
    digitalWrite(BWD_PIN, 0);
  }
  if ((DIR == 127) || (PWR == 0)) {
    digitalWrite(FWD_PIN, 0);
    digitalWrite(BWD_PIN, 0);
  }
  if (DIR == 255) {
    if (PWR == 127) {
      digitalWrite(BWD_PIN, 1);
      analogWrite (FWD_PIN, PWR);
    } else {
      digitalWrite(FWD_PIN, 0);
      analogWrite (BWD_PIN, PWR);
    }
  }
}

void loop() {
  if (!LATCH) {
    if ((millis() - tim) > 150) {
      LATCH = 1;
      motorSetup(127, 127, 0);
      Serial.println("CONNECTION ABORT!");
    }
  }
  if (mySerial.available()) {
    byte incoming = mySerial.read();
    if (incoming == STX) {
      while (mySerial.available() < MSG_LEN-1) {}
      msg[0] = STX;
      for (int i=1; i<MSG_LEN; i++) {
        msg[i] = mySerial.read();
      }
      
      if (msg[4] == ETX) {
        byte CRC = (msg[1] + msg[2] + msg[3]) & 0xFF;
        if (CRC == msg[5]) {
          tim = millis();
          LATCH = 0;
          motorSetup(msg[1],msg[2],msg[3]);
        }
      }
      //for (int i=0; i<MSG_LEN; i++) {Serial.print( (int) msg[i]); Serial.print(" ");} Serial.println(); //debug
    }
  }
}
