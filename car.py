#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial
import pygame
from time import sleep
import sys

from threading import Thread
from pygame import *
from def_render_keyboard import render_keyboard

TTY = "/dev/ttyUSB0"

WIN_WIDTH = 560
WIN_HEIGHT = 600
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"

CAR_LR = 127
CAR_DIR = 127
CAR_PWR = 0

TTY = sys.argv[1] if (sys.argv.__len__()>1) else TTY

ser = serial.Serial(TTY,9600);
ser.close()
ser.open()

def send(ser, CAR_LR, CAR_DIR, CAR_PWR):
	CRC = (CAR_LR + CAR_DIR + CAR_PWR) & 0xFF
	ba = bytearray([0x02, CAR_LR, CAR_DIR, CAR_PWR, 0x03, CRC])
	ser.write(str(ba))

class Serial_Beacon(Thread):

	def __init__(self):
		Thread.__init__(self)

	def run(self):
		while (1):
			send(ser, CAR_LR, CAR_DIR, CAR_PWR)
			sleep(0.1)

def main(TTY):
	pygame.init()
	font.init()
	v_Font = font.Font("./FreeMonoBold.ttf", 20)
	t_Lock = v_Font.render("Lock", True, Color("#FFFF00") )
	screen = pygame.display.set_mode(DISPLAY)
	pygame.display.set_caption("Car")
	bg = Surface((WIN_WIDTH,WIN_HEIGHT))
	bg.fill(Color(BACKGROUND_COLOR))

	global CAR_LR
	global CAR_DIR
	global CAR_PWR

	CC_LOCK = False
	CC_PWR = 0

	KEYS = {'W': False, 'S': False, 'A': False, 'D': False, 'R': False, 'F': False, 'Shift': False, 'Space': False}

	OLD = [CAR_LR,CAR_DIR,CAR_PWR]

	arr_act = Surface((100,51));
	draw.polygon(arr_act, Color("#FF7700"), ((0,25),(25,0),(25,15),(100,15),(100,35),(25,35),(25,50)) )
	arr_pas = Surface((100,51));
	draw.polygon(arr_pas, Color("#777777"), ((0,25),(25,0),(25,15),(100,15),(100,35),(25,35),(25,50)) )

	ntr = Surface((50,50));
	brk = Surface((20,20));
	pwr = Surface((50,5));

	tsk01 = Serial_Beacon()
	tsk01.daemon = True
	tsk01.start()

	while 1:
		for e in pygame.event.get():
			if e.type == QUIT:
				raise SystemExit, "QUIT"
			if e.type == KEYDOWN and e.key == K_ESCAPE:
				raise SystemExit, "QUIT"

			if e.type == KEYDOWN and e.key == K_w:
				KEYS['W'] = True
			if e.type == KEYUP and e.key == K_w:
				KEYS['W'] = False
			if e.type == KEYDOWN and e.key == K_s:
				KEYS['S'] = True
			if e.type == KEYUP and e.key == K_s:
				KEYS['S'] = False
			if e.type == KEYDOWN and e.key == K_a:
				KEYS['A'] = True
			if e.type == KEYUP and e.key == K_a:
				KEYS['A'] = False
			if e.type == KEYDOWN and e.key == K_d:
				KEYS['D'] = True
			if e.type == KEYUP and e.key == K_d:
				KEYS['D'] = False
			if e.type == KEYDOWN and e.key == K_r:
				KEYS['R'] = True
				CC_LOCK = True
				CC_PWR += 17
			if e.type == KEYUP and e.key == K_r:
				KEYS['R'] = False
			if e.type == KEYDOWN and e.key == K_f:
				KEYS['F'] = True
				CC_LOCK = True
				CC_PWR -= 17
			if e.type == KEYUP and e.key == K_f:
				KEYS['F'] = False
			if e.type == KEYDOWN and e.key == K_LSHIFT:
				KEYS['Shift'] = True
			if e.type == KEYUP and e.key == K_LSHIFT:
				KEYS['Shift'] = False
			if e.type == KEYDOWN and e.key == K_SPACE:
				KEYS['Space'] = True
			if e.type == KEYUP and e.key == K_SPACE:
				KEYS['Space'] = False

		if CC_LOCK:
			if CC_PWR >  255: CC_PWR =  255
			if CC_PWR < -255: CC_PWR = -255
			if CC_PWR < 0:
				CAR_PWR = -CC_PWR
				CAR_DIR = 0
			else:
				CAR_PWR = CC_PWR
				CAR_DIR = 255
			if CC_PWR == 0:
				CAR_PWR = 0
				CAR_DIR = 127
				CC_LOCK = False

		screen.blit(bg,(0,0))

		render_keyboard(screen, 10, 300, KEYS)

		if KEYS['W']^KEYS['S']:
			if KEYS['W']:
				CAR_DIR = 255
				CAR_PWR = 127
			if KEYS['S']:
				CAR_DIR =   0
				CAR_PWR = 127
			CC_LOCK = False
			CC_PWR = 0
		else:
			if not(CC_LOCK):
				CAR_DIR = 127
				CAR_PWR =   0

		if KEYS['A']^KEYS['D']:
			if KEYS['A']: CAR_LR =   0
			if KEYS['D']: CAR_LR = 255
		else:
			CAR_LR  = 127

		if KEYS['Shift']:
			if CAR_DIR != 127:
				CAR_PWR = 255
			CC_LOCK = False
			CC_PWR = 0

		if KEYS['Space']:
			CAR_DIR = 127
			CAR_PWR =   0
			CAR_LR  = 127
			CC_LOCK = False
			CC_PWR = 0

		if CAR_DIR == 255:
			fwd_arr = transform.rotate(arr_act, 270)
		else:
			fwd_arr = transform.rotate(arr_pas, 270)
		if CAR_DIR ==   0:
			bwd_arr = transform.rotate(arr_act,  90)
		else:
			bwd_arr = transform.rotate(arr_pas,  90)
		if CAR_LR  ==   0:
			lft_arr = transform.rotate(arr_act,   0)
		else:
			lft_arr = transform.rotate(arr_pas,   0)
		if CAR_LR  == 255:
			rgt_arr = transform.rotate(arr_act, 180)
		else:
			rgt_arr = transform.rotate(arr_pas, 180)
		screen.blit(lft_arr, ( 11, 120));
		screen.blit(bwd_arr, (121, 180));
		screen.blit(rgt_arr, (180, 120));
		screen.blit(fwd_arr, (120,  10));

		if (CAR_PWR == 127):
			draw.circle(ntr, Color("#777777"), (25,25), 20)
		else:
			draw.circle(ntr, Color("#FF7700"), (25,25), 20)
		screen.blit(ntr, (121, 120));

		if CAR_DIR == 255:
			for i in range(15):
				if i<(CAR_PWR/17):
					pwr.fill(Color("#FF7700"))
				else:
					pwr.fill(Color("#777777"))
				screen.blit(pwr, (310,138-i*7))
			pwr.fill(Color("#777777"))
			for i in range(15):
				screen.blit(pwr, (310,145+i*7))
		else:
			for i in range(15):
				if i<(CAR_PWR/17):
					pwr.fill(Color("#FF7700"))
				else:
					pwr.fill(Color("#777777"))
				screen.blit(pwr, (310,145+i*7))
			pwr.fill(Color("#777777"))
			for i in range(15):
				screen.blit(pwr, (310,138-i*7))

		if CC_LOCK:
			draw.circle(brk, Color("#FF7700"), (10,10), 10)
		else:
			draw.circle(brk, Color("#777777"), (10,10), 10)
		screen.blit(brk,    (310, 255));
		screen.blit(t_Lock, (335, 255));

		t_pwr = v_Font.render( str(CAR_PWR), True, Color("#FFFF00") )
		screen.blit(t_pwr,  (335 - v_Font.size(str(CAR_PWR))[0]/2, 17));

		pygame.display.update()

		if (OLD[0] != CAR_LR) or (OLD[1] != CAR_DIR) or (OLD[2] != CAR_PWR):
			send(ser, CAR_LR, CAR_DIR, CAR_PWR)
			OLD = [CAR_LR,CAR_DIR,CAR_PWR]

if __name__ == "__main__":
    main(TTY)

ser.close()
